leap_year = int(input("Please input a year: \n"))

if leap_year <= 0 :
    print("Please input a valid year")
elif leap_year % 4 == 0 :
    print(f"{leap_year} is a leap year")
else :
    print(f"{leap_year} is not a leap year")

row = int(input("Enter number of rows \n"))

if row <= 0 :
    print("Please input a valid number")
else :
    row1 = row

col = int(input("Enter number of columns \n"))

if col <= 0 :
    print("Please input a valid number")
else :
    col1 = col

i = 0

for x in range(row1):
        for y in range(col1):
            print("*", end="")
        print()
