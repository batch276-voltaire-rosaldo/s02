# Python also allows user to input, with this, users can give inputs to the program

# [Section] input
# username = input("Please enter your name: ")
# print(f"Hello {username}! Welcome to python short course!")


# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))
# print(f"The sum of num1 and num2 is {num1+num2}")

#[Section] With the user inputs, users can give inputs for the program to be used to control the application using control structures.

# Control structures can be divided into selection and repitition control structures

# Selection control structures allows the program to choose among choices and run specific codes depending on the choice taken(conditions)

# Repitition control structures allow the program to repeat certain blocks of code given a starting condition and termination condition

#[Section] If-else statements
# if-else statements

# test_num = 75

# if test_num >= 60 :
#     print("Test passed!")
# else:
#     print("Test failed!")

# trial = 55 > 40
# print(trial)

# Note that in python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed.

#[Section] if else chains can also be used to have more than 2 choices for the program

# test_num2 = int(input("Please enter the second number\n"))

# if test_num2 > 0 :
#     print("The number is positive!")
#     print(f"The number you type is {test_num2}")
# elif test_num2 == 0 :
#     print("The number is equal to 0!")
# else:
#     print("The number is negative!")

# Mini Exercise 1:

# test_num3 = int(input("Please type in the number:\n"))

# if test_num3 % 5 == 0 and test_num3 % 3 == 0 :
#     print("The number is divisible by both 5 and 3!")
# elif test_num3 % 3 == 0 :
#     print("The number is divisible by 3!")
# elif test_num3 % 5 == 0 :
#     print("The number is divisible by 5!")
# else:
#     print("The number is not divisible by 3 nor 5!")

# [Section] Loops

# i = 0

# while i <= 5 :
#     print(f"Current value of i is {i}")
#     i += 1

#[Section] For Loops are used for iteration over a sequence

# fruits = ["apple", "banana", "cherry"] #lists

# for indiv_fruit in fruits :
#     print(f"The fruit is {indiv_fruit}")

# [Section] range() method

# for x in range(6): # default start value is 0
#     print(f"x is {x}!")

# for x in range(6, 20): # start is 6, 20 is limiting value
#     print(f"x is {x}!")

# for x in range(6, 20, 2): # start is 6, 20 is limiting value, 2 is the incrementing value
#     print(f"x is {x}!")

#[Section] Break statement

# j = 0
# while j < 6 :
#     print(j)
    
#     if j == 3 :
#         break
#     j += 1

# [Section] Continue statement

k = 1

while k < 6 :
    
    k += 1
    if k == 3 :
        continue
    print(k)

